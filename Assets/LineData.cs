﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class LineData
{
    public List<Vector3> pos;
    public Vector3 LastPos()
    {
        return pos[pos.Count - 1];
    }
    public LineData()
    {
        pos = new List<Vector3>();
    }
}
