﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tango;
using System;
using System.Linq;
using System.Net.Sockets;

using System.Text;

public class ContView : MonoBehaviour, ITangoLifecycle {
    //public GlobalData GlobalData;
    public UnityEngine.UI.Button BtStart;
    public LineRenderer line;
    public LineRenderer PlanLine;
    bool isRunning = false;
    LineData lineData=new LineData();
    float yAve = 0;
    
    private UdpClient udpclient;

    const float posDistance = 0.1f;
    // Use this for initialization
    public void OnTangoPermissions(bool permissionsGranted)
    {
        ADFSelectorHelper.OnPermit(permissionsGranted);
    }
    const float trajStep = 0.3f; 
    private Vector3[] angleToTrj(float angle)
    {
        const float step =trajStep;
        const int nofStep = 10;
        const float camToRear = 0.5f;
        const float frontToRear = 0.4f;
        Vector3[] ret = Enumerable.Repeat<Vector3>(new Vector3(), nofStep ).ToArray(); ;
        Vector3 basePos = ADFSelectorHelper.poseCont.transform.position; 
        //ret[0] = basePos;
        Quaternion baseRot = ADFSelectorHelper.poseCont.transform.rotation;
        Vector3 erot = baseRot.eulerAngles;
        erot.Scale(Vector3.up);
        baseRot = Quaternion.Euler(erot);
        basePos.y = yAve ;
        basePos -= baseRot * Vector3.forward * camToRear;//Center is rea wheel
        if (Math.Abs(angle) > 0.001)
        {
            float clen = frontToRear / Mathf.Sin(angle / 180 * Mathf.PI);//length from rotation center
            float diffAngle = 360 * step / (Mathf.PI * clen * 2);
            Vector3 diffPos = new Vector3(
                Mathf.Cos(diffAngle / 180 * Mathf.PI) * clen - clen,
                0,
                Mathf.Sin(diffAngle / 180 * Mathf.PI) * clen
                );

            for (int i = 0; i < nofStep; i++)
            {
                basePos += baseRot * diffPos;
                erot.y += diffAngle; baseRot = Quaternion.Euler(erot);
                ret[i ] = basePos + baseRot * Vector3.forward * camToRear;
            }
        }
        else
        {
            for (int i = 0; i < nofStep; i++)
            {
                basePos += baseRot * (step * Vector3.forward);
               ret[i]= basePos + baseRot * Vector3.forward * camToRear;
            }
        }
        return ret;

    }
    private void Awake()
    {
        ADFSelectorHelper.OnStart(this);
    }

    void Start()
    {
        udpclient = new UdpClient(13530);
        udpclient.EnableBroadcast = true;

        
        BtStart.onClick.AddListener(this.OnStart);


        //Load Line
        string path = Application.persistentDataPath + "/" + "pos.json";
        lineData = JsonUtility.FromJson<LineData>(System.IO.File.ReadAllText(path));
        line.positionCount = lineData.pos.Count;
        line.SetPositions(lineData.pos.ToArray());
        if (lineData.pos.Count>0) { 
            yAve = 0;
            foreach (Vector3 v in lineData.pos)
            {
                yAve += v.y;
            }
            yAve /= lineData.pos.Count;
        }
    }
    // Update is called once per frame
    private int frameCount = 0;
    private float goAngle=0;
    private float speed = 0;
    private bool pathFound = false;
    private void Vec3To2(Vector3 v3,out Vector2 v2)
    {
        v2.x = v3.x;
        v2.y = v3.z;
    }
    private bool runGetAngle=false;

    IEnumerator getAngle()
    {
        if (runGetAngle) yield break;
        runGetAngle = true;
        while (runGetAngle)
        {
            float bestScore = 1e+10f;
            float bestAngle = 0;
            Vector2 d, p, td, tp;
            for (float a = -30; a < 30; a += 3)
            {
                Vector3[] v = angleToTrj(a);
                
                for(int i = 0;i< v.Length - 1; i++)
                {
                    
                    Vec3To2( v[i + 1] - v[i],out d);
                    Vec3To2(v[i],out p);
                    
                    for (int j=4;j<lineData.pos.Count -1 ; j++) {

                        Vec3To2( lineData.pos[j],out tp);
                        float len = (p - tp).magnitude;
                        if (len > 0.4f) continue;

                        Vec3To2(lineData.pos[j + 1] - lineData.pos[j], out td);
                        float diffNormalized =(td.normalized - d.normalized).magnitude;
                        if (diffNormalized > 0.3f) continue;
                        float score = (i*trajStep+2.0f)*(0.3f + len) * (1.0f + diffNormalized);
                        if (score < bestScore)
                        {
                            bestScore = score;
                            bestAngle = a;
                        }
                    }

                }
                yield return null;
            }
            if (bestScore < 0.9e+10)
            {
                goAngle = bestAngle;
                pathFound = true;
            }
            else
            {
                pathFound = false;
            }
        }
        yield break;
    }
    private float elaspedTime = 0;
    private float oldS = 94;
    void Update () {

        elaspedTime += Time.deltaTime;
        if (elaspedTime > 0.05)
        {
            if (pathFound)
            {
                speed +=0.05f;
                if (speed > 1) speed = 1;
            }
            else
            {
                speed -= 0.1f;
                if (speed < 0) speed = 0;
            }


            elaspedTime = 0;
            float s = 94 + goAngle*-1.85f;
            if (Mathf.Abs(s-oldS ) > 1 && Mathf.Abs(s-oldS )<20)
            {
                float sig=Mathf.Sign(oldS - s);
                oldS = s;
                s += sig * 20;
            }
            else
            {
                oldS = s;
            }
            float a = speed *-5;
            string data = a.ToString() + " " + s.ToString() + "\n";
            byte[] bytes = Encoding.UTF8.GetBytes(data);
            udpclient.Send(bytes, bytes.Length, "192.168.1.255", 13531);
            udpclient.Send(bytes, bytes.Length, "192.168.43.255", 13531);
            udpclient.Send(bytes, bytes.Length, "255.255.255.255", 13531);
        }
        frameCount++;
        if (isRunning)
        {

            //CAUTION angle is Degree !!!!
            if (pathFound)
            {
                Vector3[] v = angleToTrj(goAngle);
                PlanLine.positionCount = v.Length;
                PlanLine.SetPositions(v);
            }
            else
            {
                PlanLine.positionCount = 0;
                
            }

        }
        else
        {
            goAngle = 0;
            speed = 0;
        }

    }

    public void OnStart()
    {
        BtStart.gameObject.SetActive(false);
        isRunning = true;
        StartCoroutine(getAngle());

    }
    public void OnEmergencyStop()
    {
        isRunning = false;
        BtStart.gameObject.SetActive(true);
    }

    public void OnEnd()
    {
        runGetAngle = false;
        ADFSelectorHelper.OnEnd();
        UnityEngine.SceneManagement.SceneManager.LoadScene("start");
    }

    public void OnTangoServiceConnected()
    {//none
    }
    public void OnTangoServiceDisconnected()
    {//none
    }
}
