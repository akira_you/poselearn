﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tango;

public class ContLearn : MonoBehaviour, ITangoLifecycle {
    //private GlobalData GlobalData;
    // Use this for initialization
    public void OnTangoPermissions(bool permissionsGranted)
    {
    
    }
    void Start () {
        GlobalData.ADF = null; //Never use ADF when learning
        ADFSelectorHelper.Init();//Use auti start tango so don't start here
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void OnSaveEnd()
    {
        AreaDescription m_curAreaDescription = AreaDescription.SaveCurrent();
        AreaDescription.Metadata metadata = m_curAreaDescription.GetMetadata();
        metadata.m_name = GlobalData.NewADFName;
        m_curAreaDescription.SaveMetadata(metadata);
        this.OnEnd();
    }
    public void OnEnd()
    {
        ADFSelectorHelper.OnEnd();
        UnityEngine.SceneManagement.SceneManager.LoadScene("start");
    }

    public void OnTangoServiceConnected()
    {//none
    }
    public void OnTangoServiceDisconnected()
    {//none
    }
}
