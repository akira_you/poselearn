﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tango;

public class ADFSelectorHelper
{
    static public TangoApplication tangoApp = null;
    static public TangoPoseController poseCont = null;
    static public void Init()
    {
        tangoApp = Object.FindObjectOfType<TangoApplication>();
        poseCont = Object.FindObjectOfType<TangoPoseController>();

    }
    static public void OnStart(object my)
    {
        Init();
        tangoApp.Register(my);
        tangoApp.RequestPermissions();
    }
    static public void OnPermit(bool permissionsGranted)
    {
        if (permissionsGranted)
        {
            if (GlobalData.ADF == null)
            {
                tangoApp.EnableAreaDescriptions = false;
                tangoApp.UseADFOn3dr = false;
                poseCont.m_baseFrameMode = TangoPoseController.BaseFrameSelectionModeEnum.USE_START_OF_SERVICE;
                tangoApp.Startup(null);
                AndroidHelper.ShowAndroidToastMessage("Start WITHOUT ADF:" + GlobalData.ADFName);
            }
            else
            {
                tangoApp.EnableAreaDescriptions = true;
                tangoApp.UseADFOn3dr = true;
                poseCont.m_baseFrameMode = TangoPoseController.BaseFrameSelectionModeEnum.USE_AREA_DESCRIPTION; 
                tangoApp.Startup(GlobalData.ADF);
                AndroidHelper.ShowAndroidToastMessage("Start with ADF:" + GlobalData.ADFName );
            }
        }
        else
        {
            AndroidHelper.ShowAndroidToastMessage("No permission");

        }
    }
    static public void OnEnd()
    {
        tangoApp = null;
        poseCont = null;
    }
}


    public class ADFSelector : MonoBehaviour, ITangoLifecycle
{
    AreaDescription[] list = null;
    List<string> NameList = new List<string>();

    public UnityEngine.UI.Dropdown dropdown;
    public UnityEngine.UI.InputField input;
    public TangoApplication tangoApp;
    // Use this for initialization
    void Start () {
        
        dropdown.options.Clear();
        tangoApp.Register(this);
        tangoApp.RequestPermissions();
    }
    public void OnTangoPermissions(bool permissionsGranted)
    {
        // AndroidHelper.StartTangoPermissionsActivity("Dataset Read/Write");

        dropdown.options.Clear();
        if (!permissionsGranted) return;
        int index = 0;
        int select = 0;
        dropdown.options.Add(new Dropdown.OptionData { text = "No ADF" });
        list = AreaDescription.GetList();
        if (list != null) foreach (AreaDescription ad in list)
            {
                index++;
                if (GlobalData.ADF != null && GlobalData.ADF.m_uuid == ad.m_uuid) select = index;
                dropdown.options.Add(new Dropdown.OptionData(ad.GetMetadata().m_name + "_" + ad.m_uuid));
                NameList.Add(ad.GetMetadata().m_name + "_" + ad.m_uuid); 
            }
        dropdown.value = select;
        dropdown.RefreshShownValue();
    }

    private void goCommon()
    {
        int no = dropdown.value - 1;
        GlobalData.ADF = null;
        GlobalData.ADFName = "NoADF";
        if (0 <= no)
        {
            GlobalData.ADF = list[no];
            GlobalData.ADFName = NameList[no];
        }
        GlobalData.NewADFName = input.text;
    }
    public void GoLearn()
    {
        goCommon();
        UnityEngine.SceneManagement.SceneManager.LoadScene("learn");


    }
    public void GoTraj()
    {
        goCommon();
        UnityEngine.SceneManagement.SceneManager.LoadScene("traj");

    }
    public void GoView()
    {
        goCommon();
        UnityEngine.SceneManagement.SceneManager.LoadScene("view");

    }



    // Update is called once per frame
    void Update()
    {//none
    }
    public void OnTangoServiceConnected()
    {//none
    }
    public void OnTangoServiceDisconnected()
    {//none
    }
}
