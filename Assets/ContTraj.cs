﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tango;

public class ContTraj : MonoBehaviour, ITangoLifecycle {
    //private GlobalData GlobalData;
    public UnityEngine.UI.Button BtStart;
    public LineRenderer line;
    bool isRunning = false;
    LineData lineData=new LineData();
    
    const float posDistance = 0.1f;
    // Use this for initialization
    public void OnTangoPermissions(bool permissionsGranted)
    {
        ADFSelectorHelper.OnPermit(permissionsGranted);
    }
    void Start () {
        ADFSelectorHelper.OnStart(this);
        BtStart.onClick.AddListener(this.OnStart);
    }
    Vector3 nowPos()
    {
#if UNITY_EDITOR
        float phase = Time.frameCount * 0.01f;
        return new Vector3(5*Mathf.Sin(phase),0,5* Mathf.Cos(phase) -4);


#else
        return ADFSelectorHelper.poseCont.gameObject.transform.position;
#endif
    }
    void addPos(Vector3  p)
    {
        lineData.pos.Add(p);
        line.positionCount=lineData.pos.Count;
        line.SetPositions(lineData.pos.ToArray());
        
    }
    // Update is called once per frame
    void Update () {
        if (isRunning)
        {
            Vector3 p = nowPos();
            if (Vector3.Distance(lineData.LastPos(), p) > posDistance) addPos(p);
        }
	}

    public void OnStart()
    {
        BtStart.gameObject.SetActive(false);
        isRunning = true;
        lineData.pos.Add(this.nowPos());
        
    }
    public void OnSaveEnd()
    {
        //saveTrajtory
        string path=Application.persistentDataPath + "/" + "pos.json";
        Debug.Log(path);
        
        System.IO.File.WriteAllText(path, JsonUtility.ToJson(lineData));
        this.OnEnd();
    }
    public void OnEnd()
    {
        ADFSelectorHelper.OnEnd();
        UnityEngine.SceneManagement.SceneManager.LoadScene("start");
    }

    public void OnTangoServiceConnected()
    {//none
    }
    public void OnTangoServiceDisconnected()
    {//none
    }
}
